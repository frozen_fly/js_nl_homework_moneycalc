const salaryInput = document.getElementById('income-salary');
const freelanceInput = document.getElementById('income-freelance');
const extraInput1 = document.getElementById('income-extra-1');
const extraInput2 = document.getElementById('income-extra-2');

const rentInput = document.getElementById('costs-flat');
const houseServInput = document.getElementById('costs-house-services');
const transportInput = document.getElementById('costs-transport');
const creditInput = document.getElementById('costs-credit');

const totalMonthOutput = document.getElementById('total-month');
const totalDayOutput = document.getElementById('total-day');
const totalYearOutput = document.getElementById('total-year');

const rangeInput = document.querySelector('.range-input');
const rangeOutput = document.querySelector('.range-output');

const accumulation = document.getElementById('accumulation');
const spend = document.getElementById('spend');

const totalInput = [salaryInput, freelanceInput, extraInput1, extraInput2, rentInput, houseServInput, transportInput, creditInput, rangeInput];

totalInput.forEach(item => {
    item.addEventListener('input', () => {
        totalMonthOutput.value = 
        Number(salaryInput.value) + Number(freelanceInput.value) + Number(extraInput1.value) + Number(extraInput2.value) -
        Number(rentInput.value) - Number(houseServInput.value) - Number(transportInput.value) - Number(creditInput.value);
        if (totalMonthOutput.value < 0) totalMonthOutput.value = 0;
        accumulation.value = Math.round(totalMonthOutput.value * (rangeInput.value / 100));
        spend.value = Math.round(totalMonthOutput.value - accumulation.value);
        totalDayOutput.value = Math.round((totalMonthOutput.value - accumulation.value) / 30);
        totalYearOutput.value = Math.round(accumulation.value * 12);
        rangeOutput.innerHTML = rangeInput.value + '%';
    });
});
